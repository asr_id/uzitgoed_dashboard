/**
 * Get variable from search part of URL
 * @param  {String}  query   Search part of url
 * @param  {String}  key     Key to look for
 * @param  {Boolean} isArray Multiple results possible
 * @return {String}          Value from querystring
 */
function getSearchVariable(query, key, isArray) {
    var q = query.substring(0, 1) === '?' ? query.substring(1) : query,
        vars = q ? q.split('&') : [],
        results = [];

    for(var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');

        if(pair[0] === key) {
            if(!isArray) {
                return unescape(pair[1]);
            }

            results.push(unescape(pair[1]));
        }
    }

    if(!isArray) {
        return null;
    }

    return results;
}

/**
 * Set or overwrite param in query string
 * @param  {String} query    Search part of url
 * @param  {String} key      Key to look for
 * @param  {String} val      Value to set
 * @return {String}          New querystring
 */
function setSearchVariable(query, key, val) {
    var escapedVal = escape(val),
        q = query.substring(0, 1) === '?' ? query.substring(1) : query,
        vars = q ? q.split('&') : [],
        newQuery = '',
        isPresent = false;

    for(var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        var part = pair[0] + '=' + (pair[0] === key ? escapedVal : pair[1]);
        newQuery += (newQuery.length === 0 ? '?' : '&') + part;

        if(pair[0] === key) {
            isPresent = true;
        }
    }

    if(!isPresent) {
        newQuery += (newQuery.length === 0 ? '?' : '&') + key + '=' + escapedVal;
    }

    return newQuery;
}

/**
 * This functions differs from setSearchVariable because this will add key=value,
 * even if an existing key with different value is present. This will not overwrite
 * the current value and is for multi value purposes.
 * @param  {String} query    Search part of url
 * @param  {String} key      Key to look for
 * @param  {String} val      Value to set
 */
function addSearchVariable(query, key, val) {
    var escapedVal = escape(val),
        pattern = new RegExp(`&?${key}=${escapedVal}`, 'i');

    if(!pattern.test(location.search)) {
        query += (query.length === 0 ? '?' : '&') + key + '=' + escapedVal;
    }

    return query;
}

/**
 * Remove search variable from query string
 * @param  {String} query    Search part of url
 * @param  {String} key      Key to look for
 * @param  {String} val      Value to remove
 */
function removeSearchVariable(query, key, val) {
    var pattern = new RegExp(`&?${key}=${escape(val)}`, 'i');

    return query.replace(pattern, '');
}

module.exports = {
    getSearchVariable,
    setSearchVariable,
    addSearchVariable,
    removeSearchVariable
};