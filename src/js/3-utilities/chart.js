var animationSpeed = 400,

    id = 0;

/**
 * Create doughnut chart animation
 * @param {Object} data 
 */
function doughnutChartAnimation(data) {
    if(data.type === 'slice') {
        // Get the total path length in order to use for dash array animation
        var pathLength = data.element._node.getTotalLength();

        id++;
    
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
            'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });
    
        // Create animation definition while also assigning an ID to the animation for later sync usage
        var animationDefinition = {
            'stroke-dashoffset': {
                id: 'anim' + id,
                // Make sure every animation has the same speed
                dur: animationSpeed * (data.value / data.totalDataSum),
                from: -pathLength + 'px',
                to: '0px',
                // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                fill: 'freeze'
            }
        };
    
        // If this was not the first slice, we need to time the animation so that it uses the end 
        // sync event of the previous animation
        if(data.index !== 0) {
            animationDefinition['stroke-dashoffset'].begin = 'anim' + (id - 1) + '.end';
        }
    
        // We need to set an initial value before the animation starts as we are not in guided mode
        // which would do that for us
        data.element.attr({
            'stroke-dashoffset': -pathLength + 'px'
        });
    
        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
    }
}

/**
 * Create bar chart animation
 * @param {Object} data 
 */
function barChartAnimation(data) {
    if(data.type === 'bar') {
        data.element.animate({
            y2: {
                dur: animationSpeed,
                from: data.y1,
                to: data.y2
            }
        });
    }
}

module.exports = {
    doughnutChartAnimation,
    barChartAnimation
};