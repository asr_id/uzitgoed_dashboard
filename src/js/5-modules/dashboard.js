import $ from 'jquery';
import Chartist from 'chartist';
import Mustache from '2-vendor/mustache.min';
import { CountUp } from '2-vendor/countup.min';
import { doughnutChartAnimation, barChartAnimation } from '3-utilities/chart';
import urlUtil from '3-utilities/url-util';

var moduleName = 'dashboard',

    jsPrefix = '.js-',

    baseSelector = jsPrefix + moduleName,

    selectors = {
        sexLeftValue: `${baseSelector}-sex-left-value`,
        sexRightValue: `${baseSelector}-sex-right-value`,
        employmentLeftValue: `${baseSelector}-employment-left-value`,
        employmentRightValue: `${baseSelector}-employment-right-value`,
        sexLegend: `${baseSelector}-sex-legend`,
        employmentLegend: `${baseSelector}-employment-legend`,
        statusLegend: `${baseSelector}-status-legend`,
        chartSex: `${baseSelector}-chart-sex`,
        chartEmployment: `${baseSelector}-chart-employment`,
        chartInvestments: `${baseSelector}-chart-investments`,
        chartStatus: `${baseSelector}-chart-status`
    },
    
    legendTempl = `
        <li class="legend__item">
            <span class="legend__color" style="background-color: {{color}}"></span>
            {{label}}
        </li>`;

/**
 * Get total sum of array
 * @param {Number} total 
 * @param {Number} num 
 */
function getSum(total, num) {
    return total + num;
}

/**
 * Get percentage of number
 * @param {Number} part 
 * @param {Number} total 
 */
function getPercentage(part, total) {
    return Math.round(part / total * 100) + '%';
}

/**
 * Create legend
 * @param {String} selector 
 * @param {Array} labels 
 * @param {Array} colors 
 */
function createLegend(selector, labels, colors) {
    for(let label of labels) {
        $(selector).append(Mustache.render(legendTempl, {
            color: colors.shift(),
            label: label
        }));
    }
}

/**
 * Create series object for chart
 * @param {Array} values 
 * @param {Array} classNames 
 */
function createSeriesObject(values, classNames) {
    if(!classNames || classNames.length === 0) {
        return values;
    }

    var series = [];

    for(let value of values) {
        series.push({
            value: value,
            className: classNames.shift()
        });
    }

    return series;
}

/**
 * Create doughnut chart
 * @param {String} selector 
 * @param {Array} values 
 * @param {Array} classNames 
 * @param {Object} options 
 */
function createDoughnutChart(selector, values, classNames, options) {
    var chart = new Chartist.Pie(selector, {
        series: createSeriesObject(values, classNames)
    }, {
        donut: true,
        ...options
    }); 

    chart.on('draw', doughnutChartAnimation);
}

/**
 * Create sex chart
 */
function createSexChart(data) {
    var options = {
            donutWidth: 16,
            showLabel: false
        },
        total = data.data.reduce(getSum);

    createDoughnutChart(selectors.chartSex, data.data, ['orange', 'yellow'], options);

    $(selectors.sexLeftValue).text(getPercentage(data.data[1], total));
    $(selectors.sexRightValue).text(getPercentage(data.data[0], total));

    createLegend(selectors.sexLegend, data.labels.reverse(), ['#fff3cb', '#ffbd00']);
}

/**
 * Create employment chart
 */
function createEmploymentChart(data) {
    var options = {
        donutWidth: 16,
        showLabel: false
    },
    total = data.data.reduce(getSum);

    createDoughnutChart(selectors.chartEmployment, data.data, ['darkgray', 'lightgray'], options);

    $(selectors.employmentLeftValue).text(getPercentage(data.data[1], total));
    $(selectors.employmentRightValue).text(getPercentage(data.data[0], total));

    createLegend(selectors.employmentLegend, data.labels.reverse(), ['#f6f6f7', '#959497']);
}

/**
 * Create investments chart
 */
function createInvestmentsChart(data) {
    var chart = new Chartist.Bar(selectors.chartInvestments, {
        labels: data.labels,
        series: createSeriesObject(data.data)
    }, {
        distributeSeries: true,
        axisY: {
            offset: 75,
            labelInterpolationFnc: function skipLabels(value, index) {
                return index % 2  === 0 ? `€ ${value},-` : null;
            }
        }
    });   
    
    chart.on('draw', barChartAnimation);
}

/**
 * Create status chart
 */
function createStatusChart(data) {
    var total = data.data.reduce(getSum),
        options = {
            donutWidth: 75,
            chartPadding: 30,
            labelOffset: 50,
            labelDirection: 'explode',
            labelInterpolationFnc: (value) => {
                return getPercentage(value, total);
            }
        };

    createDoughnutChart(selectors.chartStatus, data.data, ['orange', 'yellow', 'darkgray', 'lightgray'], options);

    createLegend(selectors.statusLegend, data.labels, ['#ffbd00', '#fff3cb', '#959497', '#f6f6f7']);
}

/**
 * Get graph data for type
 * @param {Object} graphs 
 * @param {String} type 
 */
function getGraphData(graphs, type) {
    return graphs.find((graph) => graph.type === type);
}

/**
 * Set participants
 * @param {Object} data 
 */
function setParticipants(data) {
    new CountUp('participants-counter', data.data[0], {
        separator: '.'
    }).start();
}

/**
 * Get all chart data and create charts
 */
function createCharts(resp) {
    var response = resp.mappedResponse;

    if(response.status !== 0) {
        return;
    }

    var graphs = response.data.graphs;

    setParticipants(getGraphData(graphs, 'participants'));
    createSexChart(getGraphData(graphs, 'sex'));
    createEmploymentChart(getGraphData(graphs, 'employment'));
    createInvestmentsChart(getGraphData(graphs, 'investments'));
    createStatusChart(getGraphData(graphs, 'status'));
}

/**
 * Get chart data
 */
function getData() {
    var id = urlUtil.getSearchVariable(location.search, 'id') || '';

    $.ajax({
        url: `http://demo.aladin.business.finl.fortis/${id}`,
        type: 'GET'
    })
        .done(createCharts);
}

getData();