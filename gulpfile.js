var browserSync       = require('browser-sync').create();
var gulp              = require('gulp');
var sass              = require('gulp-sass');
var postcss           = require('gulp-postcss');
var cssNano           = require('gulp-cssnano');
var flatten           = require('gulp-flatten');
var browserify        = require('browserify');
var babelify          = require('babelify');
var sourcemaps        = require('gulp-sourcemaps');
var vinylBuffer       = require('vinyl-buffer');
var vinylSourceStream = require('vinyl-source-stream');
var autoprefixer      = require('autoprefixer');
var del               = require('del');

// Delete dist folder
gulp.task('clean', () => {
    return del.sync(['./dist/**']);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', () => {
    return gulp.src('./src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(cssNano())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', () => {
    return browserify({
        paths: ['./src/js/'],
        entries: './src/js/main.js'
    })
        // Make sure we babelify to create ES5 code from ES6 code
        .transform(babelify, {presets: ['@babel/preset-env']})
        // Bundle the entire dependency tree together
        .bundle()
        .on('error', console.log)
        // We need to get a vinyl source stream
        // since gulp works with streams, otherwise we can't
        // use our other packages
        .pipe(vinylSourceStream('main.js'))
        .pipe(vinylBuffer())
        // Initialize source maps
        .pipe(sourcemaps.init())
        // Write sourcemaps for this file
        .pipe(sourcemaps.write('.'))
        // Write the minified file to this destination aswell
        .pipe(gulp.dest('./dist/js'));
});

// Move all fonts in a flattened directory
gulp.task('fonts', () => {
    return gulp.src('./src/fonts/**/*')
        .pipe(flatten())
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(browserSync.stream());
});

// Move all images to dist
gulp.task('img', () => {
    return gulp.src('./src/img/**/*')
        .pipe(gulp.dest('./dist/images'))
        .pipe(browserSync.stream());
});

// Move index.html to dist
gulp.task('html', () => {
    return gulp.src('./src/*.html')
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', () => {

    browserSync.init({
        server: './dist'
    });

    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/fonts/**/*', ['fonts']);
    gulp.watch('./src/img/**/*', ['img']);
    gulp.watch('./src/*.html', ['html']);
    gulp.watch(['./dist/*.html', './dist/*.js']).on('change', browserSync.reload);
});

// Create dist
gulp.task('build', ['clean', 'sass', 'js', 'fonts', 'img', 'html']);

// Create dist with test file and and start browsersync
gulp.task('default', ['build', 'serve']);